# Generated by Django 2.2.5 on 2019-10-15 14:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('clinica', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='diagnostico',
            name='medida',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='medida_diagnostico', to='clinica.Medida', verbose_name='Medida'),
        ),
        migrations.AlterField(
            model_name='diagnostico',
            name='peso',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='peso_diagnostico', to='clinica.Peso', verbose_name='Peso'),
        ),
        migrations.AlterField(
            model_name='medida',
            name='abdomen_inferior',
            field=models.PositiveIntegerField(null=True, verbose_name='Abdomen Inferior'),
        ),
        migrations.AlterField(
            model_name='medida',
            name='abdomen_superior',
            field=models.PositiveIntegerField(null=True, verbose_name='Abdomen Superior'),
        ),
        migrations.AlterField(
            model_name='medida',
            name='cintura',
            field=models.PositiveIntegerField(null=True, verbose_name='Cintura'),
        ),
        migrations.AlterField(
            model_name='medida',
            name='circunferencia_brazo',
            field=models.PositiveIntegerField(null=True, verbose_name='Circunferencia Brazo'),
        ),
        migrations.AlterField(
            model_name='medida',
            name='circunferencia_muslo',
            field=models.PositiveIntegerField(null=True, verbose_name='Circunferencia Muslo'),
        ),
        migrations.AlterField(
            model_name='medida',
            name='paciente',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='medida_paciente', to='clinica.Paciente', verbose_name='Paciente'),
        ),
        migrations.AlterField(
            model_name='peso',
            name='agua',
            field=models.PositiveIntegerField(null=True, verbose_name='Agua'),
        ),
        migrations.AlterField(
            model_name='peso',
            name='complexion_fisiaca',
            field=models.PositiveIntegerField(null=True, verbose_name='Complexion Fisica'),
        ),
        migrations.AlterField(
            model_name='peso',
            name='edad_metabolica',
            field=models.PositiveIntegerField(null=True, verbose_name='Edad Metabolica'),
        ),
        migrations.AlterField(
            model_name='peso',
            name='grasa',
            field=models.PositiveIntegerField(null=True, verbose_name='Grasa'),
        ),
        migrations.AlterField(
            model_name='peso',
            name='masa_muscular',
            field=models.PositiveIntegerField(null=True, verbose_name='Masa Muscular'),
        ),
        migrations.AlterField(
            model_name='peso',
            name='masa_osea',
            field=models.PositiveIntegerField(null=True, verbose_name='Masa Osea'),
        ),
        migrations.AlterField(
            model_name='peso',
            name='metabolismo_basal',
            field=models.PositiveIntegerField(null=True, verbose_name='Metabolismo Basal'),
        ),
        migrations.AlterField(
            model_name='peso',
            name='paciente',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='peso_paciente', to='clinica.Paciente', verbose_name='Paciente'),
        ),
        migrations.AlterField(
            model_name='peso',
            name='peso',
            field=models.PositiveIntegerField(null=True, verbose_name='Peso'),
        ),
    ]
