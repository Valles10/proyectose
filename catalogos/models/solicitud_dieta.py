from django.contrib.auth.models import User
from django.db import models

from catalogos.models.dieta import Dieta
from django_extensions.db.models import TimeStampedModel

from catalogos.models.jornada import Jornada
from catalogos.models.personal import Personal
from catalogos.models.servicio import Servicio


class SolicitudDieta(models.Model):
    dieta_viaje = models.BooleanField(default=False, verbose_name='¿Dieta de Viaje?')

    # Relaciones
    servicio = models.ForeignKey(Servicio, verbose_name='Servicio', related_name='dieta_servicio',
                                 on_delete=models.PROTECT, null=True, blank=True)
    personal = models.ForeignKey(Personal, verbose_name='Personal', related_name='dieta_personal',
                                 on_delete=models.PROTECT, null=True, blank=True)

    jornada = models.ForeignKey(Jornada, verbose_name='Jornada', related_name='dieta_jornada', on_delete=models.PROTECT)
    usuario = models.ForeignKey(User, verbose_name='Usuario', related_name='dieta_usuario', on_delete=models.PROTECT)

    class Meta:
        app_label = 'catalogos'
        verbose_name = 'solicitud_dieta'
        verbose_name_plural = 'solicitudes_de_dieta'

    def __str__(self):
        return self.id


class DetalleSolicitudDieta(TimeStampedModel):
    precio = models.DecimalField(max_digits=18, decimal_places=4, verbose_name='Precio')

    # Relaciones
    solicitud_dieta = models.ForeignKey(SolicitudDieta, verbose_name='Solicitud de Dieta No.',
                                        related_name='detalle_solicitud', on_delete=models.PROTECT)
    dieta = models.ForeignKey(Dieta, verbose_name='Dieta', related_name='detalle_dieta', on_delete=models.PROTECT)

    class Meta:
        app_label = 'catalogos'
        verbose_name = 'detalle_solicitud'
        verbose_name_plural = 'detalles_solicitud'

    def __str__(self):
        return self.id
